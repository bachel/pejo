-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 23. Jun 2016 um 12:03
-- Server-Version: 5.6.25
-- PHP-Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `peijolive`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kunde`
--

CREATE TABLE IF NOT EXISTS `kunde` (
  `id` int(255) NOT NULL,
  `kdr` varchar(300) NOT NULL,
  `firma` varchar(255) NOT NULL,
  `strasse` varchar(255) NOT NULL,
  `plz` int(5) NOT NULL,
  `ort` varchar(255) NOT NULL,
  `hrb` varchar(255) NOT NULL,
  `ust` varchar(255) NOT NULL,
  `gewerbeschein` varchar(3000) NOT NULL,
  `tele` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `anrede` varchar(300) NOT NULL,
  `vorname_ap` varchar(255) NOT NULL,
  `nachname_ap` varchar(255) NOT NULL,
  `freigeschalten` varchar(1) NOT NULL,
  `partnerlevel` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `kunde`
--

INSERT INTO `kunde` (`id`, `kdr`, `firma`, `strasse`, `plz`, `ort`, `hrb`, `ust`, `gewerbeschein`, `tele`, `fax`, `email`, `url`, `anrede`, `vorname_ap`, `nachname_ap`, `freigeschalten`, `partnerlevel`) VALUES
(21, '1001', 'Test GmbH', 'Musterstr. 1', 74081, 'Heidelberg', 'HRB 1234 ', 'DE 1234567', './pdf/file_image_image.gif', '+49 (0)815 / 12 34 50', '+49 (0)815 / 12 34 51 ', 'hallo@mustermann.de ', 'www.muster.de', 'Herr', 'Max', 'Mustermann', '1', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `ID` int(11) NOT NULL,
  `kundennummer` varchar(8) NOT NULL,
  `pw` varchar(1000) NOT NULL,
  `freigeschalten` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `login`
--

INSERT INTO `login` (`ID`, `kundennummer`, `pw`, `freigeschalten`) VALUES
(1, '1001', '$2y$10$2QfjceVGvZWGMs2jyUcabeHYpbErBvHW1sIeq4pf1RuulMZagwCwa', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `produkte`
--

CREATE TABLE IF NOT EXISTS `produkte` (
  `ID` int(11) NOT NULL,
  `artikelnummer` varchar(7) NOT NULL,
  `produktname` varchar(255) NOT NULL,
  `kurzbeschreibung` varchar(2000) NOT NULL,
  `beschreibung` varchar(20000) NOT NULL,
  `ve` int(3) NOT NULL,
  `vbp` float NOT NULL,
  `bild` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `produkte`
--

INSERT INTO `produkte` (`ID`, `artikelnummer`, `produktname`, `kurzbeschreibung`, `beschreibung`, `ve`, `vbp`, `bild`) VALUES
(1, '10010', 'Falafel', 'Lecker-knusprige Bällchen aus Kichererbsen und frischem Gemüse, im östlichen Mittelmeerraum schon seit Jahrhunderten\r\n		geschätzt und das Fast Food des Vorderen Orients.', '<br><br><i>"Lecker"</i> wäre untertrieben!<br><br>\r\nDie Falafel ist eine uralte Speise, die im östlichen Mittelmeerraum schon seit Jahrhunderten geschätzt wird.<br>\r\nLecker-knusprige Bällchen aus Kichererbsen und frischen Gemüsen, serviert mit Hommos, Auberginen-Sesam-Mus (Babaganousch), Salat und Fladenbrot - so kann man sie im Libanon, in Israel, Jordanien, Syrien oder Ägypten an jeder Ecke genießen.<br>\r\nFalafel sind DAS kultige Fast-Food des Vorderen Orients.<br><br><br>\r\nPeijo-Falafel gibt es in drei Geschmacksrichtungen:<br><br>\r\n- Classic-Falafel<br>\r\n- Curry-Falafel<br>\r\n- Paprika-Falafel<br><br>\r\nAlle unsere Falafel werden aus Bio-Kichererbsen hergestellt.<br>\r\nAber es gibt auch reine Biofalafel von Peijo.<br>\r\nFalafel passen sich hervorragend in die neue deutsche Küche ein.<br><br><br>\r\nSie lassen sich in vielen Varianten genießen:<br><br>\r\n- mit Reis und Gemüse<br>\r\n- mit Zatsiki und Salat<br>\r\n- klassisch im Fladenbrot mit Salaten und Sesamsoße<br>\r\n- als Fingerfood auf kalten Platten<br>\r\n- in leckeren vegetarischen Wraps<br><br>\r\nDer kulinarischen Fantasie sind keine Grenzen gesetzt!<br><br>\r\n<u>Zubereitung</u><br><br>\r\nDie Zubereitung unserer Falafel ist denkbar einfach. Eigentlich geht alles:<br><br>\r\n<u>Pfanne</u>: Bei mittlerer Hitze 2-3 Minuten unter mehrmaligen Wenden anbraten.<br>\r\n<u>Fritteuse</u>: Bei ca. 160°C etwa 2 bis 3 Minuten frittieren.<br>\r\n<u>Backofen</u>: Bei mittlerer Hitze 8 Minuten garen.<br>\r\n<u>Mikrowelle</u>: Auf mittlerer Stufe ca. 1 Minute erhitzen.<br>\r\n<u>Konvektomat</u>: 3-5 Minuten erhitzen.<br>\r\n<u>Grill</u>: 2-4 Minuten grillen.<br>', 24, 23.99, 'img/beutel_falafel.png'),
(2, '10011', 'Bio-Falafel frisch', 'Alle unsere Falafel werden aus Bio-Kichererbsen hergestellt, aber es gibt auch reine Biofalafel von Peijo. Weil Abwechslung der beste Koch ist, gibt es unsere Falafel in drei Geschmacksrichtungen!', '<br><br>Weil Abwechslung der beste Koch ist, gibt es unsere Falafel in drei Geschmacksrichtungen:<br><br>\r\n- Classic (typisch arabisch mit Petersilie, Kreuzkümmel und Koriander)<br>\r\n- Curry (mit einer feinen, selbst hergestellten Currymischung)<br>\r\n- Paprika (mit frischer roter Paprika, leicht pikant gewürzt)<br><br><br>\r\n<u>Zubereitung</u><br><br>\r\nDie Zubereitung unserer Falafel ist denkbar einfach. Eigentlich geht alles:<br><br>\r\n<u>Pfanne</u>: Bei mittlerer Hitze 2-3 Minuten unter mehrmaligen Wenden anbraten.<br>\r\n<u>Fritteuse</u>: Bei ca. 160°C etwa 2 bis 3 Minuten frittieren.<br>\r\n<u>Backofen</u>: Bei mittlerer Hitze 8 Minuten garen.<br>\r\n<u>Mikrowelle</u>: Auf mittlerer Stufe ca. 1 Minute erhitzen.<br>\r\n<u>Konvektomat</u>: 3-5 Minuten erhitzen.<br>\r\n<u>Grill</u>: 2-4 Minuten grillen.<br>', 24, 20.99, 'img/falafel_dreier.png'),
(3, '10012', 'Bio-Falafel Fertigmischung', 'Immer Falafel im Haus! Frisch angerührt und in der Pfanne angebraten sind unsere Falafel ein ebenso großer Genuß wie unsere frischen Falafel aus der Kühltheke.', '<br><br>Immer Falafel im Haus!<br><br>\r\nFrisch angerührt und in der Pfanne angebraten sind unsere Falafel ein ebenso großer Genuß wie unsere frischen Falafel aus der Kühltheke.<br><br>\r\nBei der Rezeptur unserer drei Fertigmischungen in den gewohnten Geschmacksrichtungen Classic, Curry und Paprika haben wir besonders viel Sorgfalt darauf verwendet, den guten Geschmack der frischen Falafel auch in der Fertigmischung zu verwirklichen.<br>\r\nDazu kommt noch der besondere Knusper-Effekt, den die Falafel haben, wenn sie frisch aus der Pfanne kommen. Probieren Sie es aus...<br><br><br>\r\n<u>Zubereitung</u><br><br>\r\nInhalt des Beutels in 180ml Wasser verrühren und ca. 45 Minuten quellen lassen. Danach noch einmal gründlich durch rühren.<br>\r\nDie Masse sollte jetzt locker vom Löffel rutschen, aber nicht zu flüssig sein. Bei Bedarf noch Wasser hinzufügen.<br>\r\nNun die Masse portionsweise mit einem Löffel in eine Pfanne mit reichlich Öl geben und bei mittlerer Hitze von beiden Seiten knusprig goldbraun anbraten. Alternativ kann man die Masse auch auf einmal in die Pfanne geben und einen großen Fladen backen, den man danach in kleine Stücke zerschneidet.<br>\r\nDie fertigen Falafel mit Hommos, Sesamsoße oder Auberginen-Sesam-Mus und Salaten servieren.<br>', 24, 20.99, 'img/falafel_fertigmischung.jpeg'),
(4, '10013', 'Hummus (Kichererbsen-Sesam-Mus)', 'Zubereitet aus feinen Bio-Kichererbsen und leckerem hausgemachten Bio-Sesam-Mus (Tahine) stellt unser Bio-Hummus ein besonderes Highlight der veganen libanesischen Küche dar.', '<br><br>Hummus, Houmous, Hommos, Humus,...<br>\r\nviele Schreibweisen - für ein und dieselbe leckere Speise!<br><br>\r\nZubereitet aus feinen Bio-Kichererbsen und leckerem hausgemachten Bio-Sesam-Mus (Tahine) stellt unser Bio-Hummus ein besonderes Highlight der veganen libanesischen Küche dar.<br><br>\r\nFein-cremig, leicht nussig und - für Freunde der Kichererbse - undendlich lecker, hat unser Hummus definitiv Suchtpotenzial!<br><br>\r\nErhältlich in teilnehmenden Naturkostfachgeschäften und Reformhäusern sowie online unter: <br><br>\r\nwww.vegan-wonderland.de<br>\r\nwww.veganbasics.de<br>\r\nwww.petastore.de<br>\r\nwww.veggiepro.de<br><br><br>\r\n<u>Zutaten</u><br><br>\r\nKichererbsen*, Wasser, Sesamsaat*, Rapsöl*, Meersalz, Zitronensäure, Knoblauch*<br><br>\r\n(*Zutaten aus kontrolliert biologischem Anbau)<br>', 24, 20.99, 'img/hommos.png');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `kunde`
--
ALTER TABLE `kunde`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`ID`);

--
-- Indizes für die Tabelle `produkte`
--
ALTER TABLE `produkte`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `artikelnummer` (`artikelnummer`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `kunde`
--
ALTER TABLE `kunde`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT für Tabelle `login`
--
ALTER TABLE `login`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT für Tabelle `produkte`
--
ALTER TABLE `produkte`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
