<?php
// Verbindung und Funktionen Händler Tabelle
// Ändern von Einträgen Händler Tabelle
if(isset($_POST['bearbeiten'])){
				$id = $_POST['bearbeiten'];
				$kdr = $_POST['kdr'];
				$firma = $_POST['firma'];
				$strasse = $_POST['strasse'];
				$plz = $_POST['plz'];
				$ort = $_POST['ort'];
				$hrb = $_POST['hrb'];
				$ust = $_POST['ust'];
				$tele = $_POST['tele'];
				$fax = $_POST['fax'];
				$email = $_POST['email'];
				$url = $_POST['url'];
				$anrede = $_POST['anrede'];
				$vorname_ap = $_POST['vorname_ap'];
				$nachname_ap = $_POST['nachname_ap'];
				$freigeschalten = $_POST['freigeschalten'];	
            $sql1 = "UPDATE kunde SET kdr=?, firma=?, strasse=?, plz=?, ort=?, hrb=?, ust=?, tele=?, fax=?, email=?, url=?, 
				         anrede=?, vorname_ap=?, nachname_ap=?, freigeschalten=? WHERE id=?"; 
	         $prepare = mysqli_prepare($verbindung, $sql1);
	         mysqli_stmt_bind_param($prepare, 'sssssssssssssssi', $kdr, $firma, $strasse, $plz, $ort, $hrb, $ust, $tele, $fax,
		      $email, $url, $anrede, $vorname_ap, $nachname_ap, $freigeschalten, $id);
	         mysqli_stmt_execute($prepare);

}
// Löschen von Einträgen Händler Tabelle
if(isset($_POST['loeschen']) and is_numeric($_POST['loeschen'])){
	$sql2 = "DELETE FROM `kunde` WHERE id=".$_POST['loeschen'];
	$records = mysqli_query($verbindung,$sql2);
	}
// Neuer Händler Anlegen
if(isset($_POST['neukd'])){
		      $kdr = $_POST['kdr'];
				$firma = $_POST['firma'];
				$strasse = $_POST['strasse'];
				$plz = $_POST['plz'];
				$ort = $_POST['ort'];
				$hrb = $_POST['hrb'];
				$ust = $_POST['ust'];
				$tele = $_POST['tele'];
				$fax = $_POST['fax'];
				$email = $_POST['email'];
				$url = $_POST['url'];
				$anrede = $_POST['anrede'];
				$vorname_ap = $_POST['vorname_ap'];
				$nachname_ap = $_POST['nachname_ap'];
				$freigeschalten = $_POST['freigeschalten'];
		      $gewerbe = '';
		      $Dateiname = 'Keine Datei verfügbar.' ;
	  
	  try {
			// Fehler bei fehlender oder fehlerhafter Dateiübertragung:
			if( !isset( $_FILES['Datei']['error'] ) || is_array( $_FILES['Datei']['error'] ) ) {
				throw new RuntimeException('Fehlender Dateiupload') ;
			}
			// Auswertung vorhandener Fehlerzustände
			switch( $_FILES['Datei']['error'] ) {
				case 0 : break ; // Alles OK
				case 1 : 
				case 2 : throw new RuntimeException('Datei zu groß!') ;
				case 3 : throw new RuntimeException('Übertragung unvollständig.') ;
				case 4 : throw new RuntimeException('Keine Datei empfangen.') ;
				case 6 : throw new RuntimeException('Kein temporäres Verzeichnis') ;
				case 7 : throw new RuntimeException('Lokale Speicherung unmöglich.') ;
				case 8 : throw new RuntimeException('Erweiterung unzulässig.') ;
				default: throw new RuntimeException('Unbekannte Störung des Uploads.') ;
			}
			// Maßnahmen zur Kontrolle / Reduktion der Datenmenge
			if( $_FILES['Datei']['size'] > 5000000 ) 
				throw new RuntimeException('Dateigröße grenzwertig');
			// Maßnahmen zur Kontrolle und Absicherung des Dateiformats bzw. MIME-Typs
			$Akzeptiert = array('jpg'=>'image/jpeg', 'gif'=>'image/gif', 'png'=>'image/png', 'pdf'=>'application/pdf') ;
			$Dateiinfo = new finfo( FILEINFO_MIME_TYPE ) ;
			$Erweiterung = array_search( $Dateiinfo->file( $_FILES['Datei']['tmp_name'] ), $Akzeptiert, true ) ;
			if( $Erweiterung === false ) { // MIME-Typ nicht in Akzeptiert
				throw new RuntimeException('Ungültiges Dateiformat.') ;
			} else { // Neuen Dateinamen aus dem alten und der ermittelten Erweiterung generieren
				$Dateiname = explode('.', $_FILES['Datei']['name'] ) ;
				$Dateiname = '../pdf/'.$Dateiname[0].'.'.$Erweiterung ;
				// Pfad anpassen
				$splitt = explode('.', $Dateiname);
				$richtigerpfad = '.'.$splitt[2].'.'.$Erweiterung;
				$gewerbe = $richtigerpfad;
			}
			if( ! move_uploaded_file( $_FILES['Datei']['tmp_name'], $Dateiname ) ) {
				throw new RuntimeException('Übertragene Datei konnte nicht in das Zielverzeichnis verschoben werden.') ;
			}
		} catch( RuntimeException $Fehler ) {
			echo('FEHLER: '.$Fehler->getMessage() ) ;
		}
	
	$sql1 = "INSERT INTO kunde (kdr, firma, strasse, plz, ort, hrb, ust, gewerbeschein, tele, fax, email, url, 
				anrede, vorname_ap, nachname_ap, freigeschalten) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";	
	$prepare = mysqli_prepare($verbindung, $sql1);
	mysqli_stmt_bind_param($prepare, 'ssssssssssssssss', $kdr, $firma, $strasse, $plz, $ort, $hrb, $ust, $gewerbe, $tele, 
	                       $fax, $email, $url, $anrede, $vorname_ap, $nachname_ap, $freigeschalten);
	mysqli_stmt_execute($prepare);
	}
// Verbindung und Funktionen Händler Tabelle
$sql2 = "SELECT * FROM `kunde` ";
$records = mysqli_query($verbindung,$sql2);
$count = mysqli_num_rows($records);
// Verbindung und Funktionen Produkte Tabelle
// Ändern von Produkte Tabelle
if(isset($_POST['bearbeitenp'])){
				$id = $_POST['bearbeitenp'];
				$artikel = $_POST['artikelnummer'];
				$Produktname = $_POST['produktname'];
				$Kbeschreibung = $_POST['kurzbeschreibung'];
				$beschreibung = $_POST['beschreibung'];
				$ve = $_POST['ve'];
				$vbp = $_POST['vbp'];
            $sql1 = "UPDATE produkte SET artikelnummer=?, produktname=?, kurzbeschreibung=?, beschreibung=?, ve=?, 
							vbp=? WHERE ID=?"; 
	         $prepare = mysqli_prepare($verbindung, $sql1);
	         mysqli_stmt_bind_param($prepare, 'isssiii', $artikel, $Produktname, $Kbeschreibung, $beschreibung, 
											  $ve, $vbp, $id);
	         mysqli_stmt_execute($prepare);

}
// Löschen von Einträgen Produkte Tabelle
if(isset($_POST['loeschenp']) and is_numeric($_POST['loeschenp'])){
	$sql2p = "DELETE FROM produkte WHERE id=".$_POST['loeschenp'];
	$records2p = mysqli_query($verbindung,$sql2p);
	}
// Neue Produkte Anlegen
if(isset($_POST['neup'])){
				$artikel = $_POST['artikelnummer'];
				$Produktname = $_POST['produktname'];
				$Kbeschreibung = $_POST['kurzbeschreibung'];
				$beschreibung = $_POST['beschreibung'];
				$ve = $_POST['ve'];
				$vbp = $_POST['vbp'];
		      $bild = '';
		      $Dateiname = 'Keine Datei verfügbar.' ;
	  
	  try {
			// Fehler bei fehlender oder fehlerhafter Dateiübertragung:
			if( !isset( $_FILES['Datei']['error'] ) || is_array( $_FILES['Datei']['error'] ) ) {
				throw new RuntimeException('Fehlender Dateiupload') ;
			}
			// Auswertung vorhandener Fehlerzustände
			switch( $_FILES['Datei']['error'] ) {
				case 0 : break ; // Alles OK
				case 1 : 
				case 2 : throw new RuntimeException('Datei zu groß!') ;
				case 3 : throw new RuntimeException('Übertragung unvollständig.') ;
				case 4 : throw new RuntimeException('Keine Datei empfangen.') ;
				case 6 : throw new RuntimeException('Kein temporäres Verzeichnis') ;
				case 7 : throw new RuntimeException('Lokale Speicherung unmöglich.') ;
				case 8 : throw new RuntimeException('Erweiterung unzulässig.') ;
				default: throw new RuntimeException('Unbekannte Störung des Uploads.') ;
			}
			// Maßnahmen zur Kontrolle / Reduktion der Datenmenge
			if( $_FILES['Datei']['size'] > 5000000 ) 
				throw new RuntimeException('Dateigröße grenzwertig');
			// Maßnahmen zur Kontrolle und Absicherung des Dateiformats bzw. MIME-Typs
			$Akzeptiert = array('jpg'=>'image/jpeg', 'gif'=>'image/gif', 'png'=>'image/png', 'pdf'=>'application/pdf') ;
			$Dateiinfo = new finfo( FILEINFO_MIME_TYPE ) ;
			$Erweiterung = array_search( $Dateiinfo->file( $_FILES['Datei']['tmp_name'] ), $Akzeptiert, true ) ;
			if( $Erweiterung === false ) { // MIME-Typ nicht in Akzeptiert
				throw new RuntimeException('Ungültiges Dateiformat.') ;
			} else { // Neuen Dateinamen aus dem alten und der ermittelten Erweiterung generieren
				$Dateiname = explode('.', $_FILES['Datei']['name'] ) ;
				$Dateiname = '../img/'.$Dateiname[0].'.'.$Erweiterung ;
				// Pfad anpassen
				$splitt = explode('.', $Dateiname);
				$richtigerpfad = '.'.$splitt[2].'.'.$Erweiterung;
				$bild = $richtigerpfad;
			}
			if( ! move_uploaded_file( $_FILES['Datei']['tmp_name'], $Dateiname ) ) {
				throw new RuntimeException('Übertragene Datei konnte nicht in das Zielverzeichnis verschoben werden.') ;
			}
		} catch( RuntimeException $Fehler ) {
			echo('FEHLER: '.$Fehler->getMessage() ) ;
		}
		
	
	$sql1 = "INSERT INTO produkte (artikelnummer, produktname, kurzbeschreibung, beschreibung, ve, vbp, bild ) 
				VALUES ( ?, ?, ?, ?, ?, ?, ?)";	
	$prepare = mysqli_prepare($verbindung, $sql1);
	mysqli_stmt_bind_param($prepare, 'isssiis', $artikel, $Produktname, $Kbeschreibung, $beschreibung, $ve, $vbp, $bild );
	mysqli_stmt_execute($prepare);
	}
// Verbindung für Produkte 
$sqlp = "SELECT * FROM `produkte`";
$recordsp = mysqli_query($verbindung,$sqlp);
$countp = mysqli_num_rows($recordsp);
// Verbindung und Funktionen Login Tabelle
// Ändern von Produkte Tabelle
if(isset($_POST['bearbeitenl'])){
				$id = $_POST['bearbeitenl'];
				$kundenummer = $_POST['kundennummer'];
				$pw = $_POST['pw'];
				$frei = $_POST['freigeschalten'];
            $sql1 = "UPDATE login SET kundennummer=?, pw=?, freigeschalten=? WHERE ID=?"; 
	         $prepare = mysqli_prepare($verbindung, $sql1);
	         mysqli_stmt_bind_param($prepare, 'issi', $kundenummer, $pw, $frei, $id);
	         mysqli_stmt_execute($prepare);

}
// Löschen von Einträgen Login Tabelle
if(isset($_POST['loeschenl']) and is_numeric($_POST['loeschenl'])){
	$sql2p = "DELETE FROM login WHERE id=".$_POST['loeschenl'];
	$records2p = mysqli_query($verbindung,$sql2p);
	}
// Neuer Login Anlegen
if(isset($_POST['neul'])){
				$kundenummer = $_POST['kundennummer'];
				$pw = password_hash($_POST['pw'], PASSWORD_DEFAULT);
				$frei = $_POST['freigeschalten'];
	 $sql1 = "INSERT INTO login (kundennummer, pw, freigeschalten ) 
				VALUES ( ?, ?, ?)";	
	$prepare = mysqli_prepare($verbindung, $sql1);
	mysqli_stmt_bind_param($prepare, 'sss', $kundenummer, $pw, $frei );
	mysqli_stmt_execute($prepare);
	}
// Verbindung für LOG 
$sqll = "SELECT * FROM `login`";
$recordsl = mysqli_query($verbindung,$sqll);
$countl = mysqli_num_rows($recordsl);
		 
?>