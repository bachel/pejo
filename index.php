<?php 
	include('module/php/master.php');
?>
<!doctype html>
<html>
<head>
<?php include('module/html/head.html');?>
</head>	
<body>


<!-- Einbindung der Dateien (HTML-Body) -->
<?php
      // Ausgabekontrolle
		if($verhalten == 0) {
				if(!isset($_GET['page'])){
					include('module/html/banner.html');
					include('module/html/navigation.html');
					include('module/html/registrierung.html');
					include('module/html/loginpopup.html');
					include('module/html/content.html');
					include('module/html/footerohnelog.html');
					include('module/html/agb.html');
					include('module/html/datenschutz.html');
					include('module/html/impressum.html');
				
				}
				
				elseif($_GET['page']  == 'index'){
					include('module/html/banner.html');
					include('module/html/navigation.html');
					include('module/html/registrierung.html');
					include('module/html/loginpopup.html');
					include('module/html/content.html');
					include('module/html/footerohnelog.html');
					include('module/html/agb.html');
					include('module/html/datenschutz.html');
					include('module/html/impressum.html');
				}
				elseif($_GET['page']  == 'about')	{
					include('module/html/banner.html');
					include('module/html/navigation.html');
					include('module/html/registrierung.html');
					include('module/html/loginpopup.html');
					include('module/html/ueberuns.html');
					include('module/html/footerohnelog.html');
					include('module/html/agb.html');
					include('module/html/datenschutz.html');
					include('module/html/impressum.html');
				}
				elseif($_GET['page']  == 'haendlersuche')	{
					include('module/html/banner.html');
					include('module/html/navigation.html');
					include('module/html/registrierung.html');
					include('module/html/loginpopup.html');
					include('module/php/haendlersuche.php');
					include('module/html/footerohnelog.html');
					include('module/html/agb.html');
					include('module/html/datenschutz.html');
					include('module/html/impressum.html');
				}
				elseif($_GET['page']  == 'contact')	{
					include('module/html/banner.html');
					include('module/html/navigation.html');
					include('module/html/registrierung.html');
					include('module/html/loginpopup.html');
					include('module/html/kontakt.html');
					include('module/html/footerohnelog.html');
					include('module/html/agb.html');
					include('module/html/datenschutz.html');
					include('module/html/impressum.html');
				}
				elseif($_GET['page']  == 'produkte')	{
					include('module/html/banner.html');
					include('module/html/navigation.html');
					include('module/html/registrierung.html');
					include('module/html/loginpopup.html');
					include('module/php/produkteohnelog.php');           
					include('module/html/footerohnelog.html');
					include('module/html/agb.html');
					include('module/html/datenschutz.html');
					include('module/html/impressum.html');
				}
				
				elseif($_GET['page']  == 'reg')	{
					include('module/html/banner.html');
					include('module/html/navigation.html');
					include('module/html/registrierung.html');
					include('module/html/loginpopup.html');
					include('module/html/content.html');            
					include('module/html/footerohnelog.html');
					include('module/html/agb.html');
					include('module/html/datenschutz.html');
					include('module/html/impressum.html');
				}
	}
		if($verhalten == 1) {
				if(!isset($_GET['page']) && isset($_GET['id'])){
					include('module/html/inwarenkorb.html');
					
					
					
					}
				
				
				elseif(!isset($_GET['page'])){
					include('module/html/banner.html');
					include('module/php/navigationmitlog.php');
					include('module/html/content.html');
					include('module/html/footer.html');
					include('module/php/warenkorb.php');
					include('module/html/agb.html');
					include('module/html/datenschutz.html');
					include('module/html/impressum.html');
					
				
				}	
					
					
					
				elseif($_GET['page'] == 'log'){
					include('module/html/banner.html');
					include('module/php/navigationmitlog.php');
					include('module/html/content.html');
					include('module/html/footer.html');
					
					include('module/php/warenkorb.php');
					include('module/html/agb.html');
					include('module/html/datenschutz.html');
					include('module/html/impressum.html');
				
				}
				
				elseif($_GET['page']  == 'index'){
					include('module/html/banner.html');
					include('module/php/navigationmitlog.php');;
					include('module/html/content.html');
					include('module/html/footer.html');
					
					include('module/php/warenkorb.php');
					include('module/html/agb.html');
					include('module/html/datenschutz.html');
					include('module/html/impressum.html');
				}
				elseif($_GET['page']  == 'about')	{
					include('module/html/banner.html');
					include('module/php/navigationmitlog.php');
					include('module/html/ueberuns.html');
					include('module/html/footer.html');
					
					
					include('module/php/warenkorb.php');
					include('module/html/agb.html');
					include('module/html/datenschutz.html');
					include('module/html/impressum.html');
				}
				elseif($_GET['page']  == 'haendlersuche')	{
					include('module/html/banner.html');
					include('module/php/navigationmitlog.php');
					include('module/html/footer.html');
					include('module/php/haendlersuche.php');
					include('module/php/warenkorb.php');
					include('module/html/agb.html');
					include('module/html/datenschutz.html');
					include('module/html/impressum.html');
				}
				elseif($_GET['page']  == 'contact')	{
					include('module/html/banner.html');
					include('module/php/navigationmitlog.php');
					include('module/html/kontakt.html');
					include('module/html/footer.html');
					
					include('module/php/warenkorb.php');
					include('module/html/agb.html');
					include('module/html/datenschutz.html');
					include('module/html/impressum.html');
				}
				
				elseif($_GET['page']  == 'produkte')	{
					include('module/html/banner.html');
					include('module/php/navigationmitlog.php');
					include('module/php/produktemitlog.php');
					include('module/html/footer.html');
					include('module/php/warenkorb.php');
					include('module/html/agb.html');
					include('module/html/datenschutz.html');
					include('module/html/impressum.html');
				}
				// Resfresh nach Löschen oder Bearbeiten Produkte in Warenkorb
				elseif($_GET['page']  == 'loeschen')	{
					header("Refresh:0; url=index.php?page=produkte");
					include('module/php/warenkorb.php');
					
					
				}
			
			


		}
		if($verhalten == 2) {
			
			
			}
		
			
			
		
		
		
	
	
	
	
?>  
</body>
</html>