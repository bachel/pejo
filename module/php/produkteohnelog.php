<?php
// Auflisten Aller Produkte in Datenbank mit Allergieinfo und Details ohne Warenkorb Funktion
while($produkt = mysqli_fetch_assoc($records)){
				$Produktname = $produkt['produktname'];
				$Kbeschreibung = $produkt['kurzbeschreibung'];
				$beschreibung = $produkt['beschreibung'];
				$bild = $produkt['bild'];
				$artikel = $produkt['artikelnummer'];
				
				$tabelle = '<div class="container">'.
							  '<div class="container-fluid">'.
							  '<div class="jumbotron">
            			  <h1 id="shopgruss"><strong>Produktpalette Piejo</strong></h1><br>
            			  <p id="shopeinleitung">Hier finden Sie Details und Allergikerinformationen über unsere Produkte.</p>
        					  </div>
        					  <hr>'.
				           '<div class="row">';
				
				$ausgabe .= '<div class="col-sm-6 col-md-6">'.
								 '<div class="thumbnail">'.
								 '<img class="produktbilder" src="'.$bild.'" alt="'.$Produktname.'">'.
								 '<div class="caption">'.
								 '<h3>'.$Produktname.'</h3>'.
								 '<p>'.$Kbeschreibung.'</p>'.
								 '<p><button id="detailbutton" type="button" class="btn btn-primary" data-toggle="modal" data-target="#'.$artikel.'">Details</button>'.
								 '&nbsp;&nbsp;&nbsp;&nbsp;'.
								 '<button type="button" class="btn btn-default" data-toggle="modal" data-target="#alla"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;&nbsp;Allergikerinfo</button>'.
								 '</p>'.
								 '</div>'.
								 '</div>'.
								 '</div>'.
								 '<div class="modal fade" id="'.$artikel.'" role="dialog">
    								<div class="modal-dialog testi">
      								<div class="modal-content">
        									<div class="modal-header">
          									<button type="button" class="close" data-dismiss="modal">&times;</button>
          										<h4 class="modal-title">'.$Produktname.'</h4>
        												<div class="modal-body">'.$beschreibung.'</div>
														<div class="modal-footer">
														<button id="buttonproduktez" type="button" class="btn btn-default" data-dismiss="modal" style="color: #ffffff;">Zurück</button>
														</div> 

   													 </div>
 															 </div>
																</div>
																</div>';
				
				$tabellez = '</div>'.
								'</div>'.
								'</div>'.
								'<div class="modal fade" id="alla" role="dialog" style="color: #000000;">
    								<div class="modal-dialog modal-lg">
										<div class="modal-content">
										  <div class="modal-header">
											 <button type="button" class="close" data-dismiss="modal">&times;</button>
											 <h4 class="modal-title">Informationen für Allergiker</h4>
										  <div class="modal-body">
								Immer mehr Menschen entwickeln im Laufe Ihres Lebens eine Unverträg-lichkeit für Gluten, Laktose oder andere allergene Stoffe, wie z.B. Erdnüsse.<br><br>
								Für sie ist es wichtig, sich beim Einkauf darauf verlassen zu können, dass die Produkte ihrer Wahl garantiert keine derartigen Allergieauslöser enthalten.<br>
								Um Ihnen die Orientierung zu erleichtern haben wir unten eine Postivliste aufgestellt, die alle Stoffe und Zutaten aufführt, die in unseren Produkten enthalten sind.<br><br>
								Sollten Sie eine Zutat finden, die Sie nicht vertragen, so müssen wir Ihnen leider davon abraten unsere Produkte zu genießen.<br>
								Sollten Sie aber nicht auf einen Problemstoff stoßen, so können wir Ihnen garantieren, dass er in unseren Produkten auch nicht enthalten ist, da wir alles bei uns im Hause selbst herstellen und daher die exakte Kontrolle über unsere Zutaten haben.<br><br>
								In unserer Produktion werden ausschließlich die Zutaten und Gewürze verwendet, die in der folgenden Liste angegeben sind (alle Angaben in alphabetischer Reihenfolge):<br><br>
								<table class="table">
								  <thead>
									 <tr>
										<th>Rohstoffe</th>
										<th>Gewürze</th>
									 </tr>
								  </thead>
								  <tbody>
									 <tr>
										<td>Aubergine</td>
										<td>Bockshornkleesaat</td>
									 </tr>
									 <tr>
										<td>Kartoffeln</td>
										<td>Thornton</td>
									 </tr>
									 <tr>
										<td>Kichererbsen</td>
										<td>Fenchel</td>
									 </tr>
									 <tr>
										<td>Knoblauch</td>
										<td>Ingwer</td>
									 </tr>
									 <tr>
										<td>Lauch</td>
										<td>Kardamon</td>
									 </tr>
									 <tr>
										<td>Mandeln</td>
										<td>Koriander</td>
									 </tr>
									 <tr>
										<td>Meersalz</td>
										<td>Kreuzkümmel (Cumin)</td>
									 </tr>
									 <tr>
										<td>Olivenöl</td>
										<td>Kümmel</td>
									 </tr>
									 <tr>
										<td>Paprika (rot und grün)</td>
										<td>Kurkuma</td>
									 </tr>
									 <tr>
										<td>Petersilie</td>
										<td>Lorbeer</td>
									 </tr>
									 <tr>
										<td>Rapsöl</td>
										<td>Muskat</td>
									 </tr>
									 <tr>
										<td>Salz</td>
										<td>Nelke</td>
									 </tr>
									 <tr>
										<td>Sesamsaat</td>
										<td>Pfeffer (schwarz)</td>
									 </tr>
									 <tr>
										<td>Tomaten</td>
										<td>Piment</td>
									 </tr>
									 <tr>
										<td>Zwiebeln</td>
										<td>Senfsaat</td>
									 </tr>
									 <tr>
										<td> </td>
										<td>Zitronensäure</td>
									 </tr>
								  </tbody>
								</table>
								
								<br><br>
								Wir weisen darauf hin, dass wir <u>keine</u> glutenhaltigen Zutaten, <u>keine</u> Hefe, und auch sonst <u>keine</u> Zutaten oder versteckten Hilfsmittel- und Stoffe verwenden, die in der obigen Liste nicht deklariert sind. In unserer Produktion werden ausschließlich vegane Produkte hergestellt.
								<br>
								</div>
								<div class="modal-footer">
									<button id="buttonai" type="button" class="btn btn-default" data-dismiss="modal" style="color: #ffffff;">Zurück</button>
								</div> 
								
									 </div>
								  </div>
								</div>
								</div>';
}

// Ausgabe Tabelle
      echo $tabelle;
		echo $ausgabe;
		echo $tabellez;  
?>
