<?php
$navigation = 
	'<nav id="navigationcss" class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Navigation</span>
          		    <span class="icon-bar"></span>
            		<span class="icon-bar"></span>
            		<span class="icon-bar"></span>
          		</button>
          		<!-- Logo Peijo -->
           	    <a class="navbar-brand" href="index.php?page=index"><img src="img/logoneu.png" id="logo" class="img-responsive"></a>      
			</div>
        <!-- Forsetzung Navigation -->
        <div id="navbar" class="collapse navbar-collapse">
        	<ul class="nav navbar-nav">
            	<li id="navabstand"><a href="index.php?page=index">Startseite</a>
            	<li><a href="index.php?page=about">Über Uns</a></li>
            	<li><a href="index.php?page=produkte">Produkte</a></li>
            	<li><a href="index.php?page=haendlersuche">Händlersuche</a></li>
            	<li><a href="index.php?page=contact">Kontakt</a></li>
          	</ul>
          	<!-- Suchfunktion Navigation -->
         	<form id="suchmaschine" class="navbar-form navbar-left" role="search">
          		<div class="form-group">
          			<input type="texst" class="form-control" placeholder="Produktinformation,...">
           		</div>
            	<button type="submit" class="btn btn-default">Suchen</button>
     	  	</form>
          	<!-- Registrier- und Loginfunktion Navigation -->
         	<ul class="nav navbar-nav navbar-right">
          		<li><button id="registrierbutton" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#warenkorb"><span class="glyphicon glyphicon-shopping-cart"></span> Warenkorb <span id="anzahl" class="badge"> '.$ik.'</span></button></li>
				<div class="logoutdiv">
            	<li><a href="module/php/logout.php" id="loginbutton" class="btn btn-info btn-lg" role="button"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
				</div>
          	</ul>
        </div>
    	</div>
    </nav>';
	 echo $navigation
?>